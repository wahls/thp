
// This is a auxiliary script for the script exa3.sce. Its purpose is to set
// various simulation parameters.

q = 2;                  // no. of channel outputs
p = 2;                  // no. of channel inputs
kappa = 5;              // McMillan degree of the channels
L = 15;                 // latency time
SNRs = -6:8;            // Signal-to-noise ratio range (in dB)
Nsymb = 1000;           // no. of transmitted data symbols per SNR and channel
Nchan = 100000;         // no. of random channels per SNR
Etr = 1;                // transmit power
tau = 2*sqrt(2);        // modulo constant
sigv = tau/sqrt(6);     // square root of the power of the modulo operators
                        // output