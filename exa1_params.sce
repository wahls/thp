
// This is a auxiliary script for the script exa1.sce. Its purpose is
// to set various simulation parameters.

q = 1;                  // no. of channel outputs
p = 1;                  // no. of channel inputs
kappa = 5;              // McMillan degree of the channels
L = 15;                 // latency time
SNRs = -30:30;          // SNR range (in dB)
Nsymb = 1000;           // no. of transmitted data symbols per SNR
                        // and channel
Npreamble = 10000;      // length of the preamble for the estimation
                        // of the scaling factor
Nchan = 100000;         // no. of random channels per SNR
Etr = 1;                // transmit power
tau = 8/sqrt(10);       // modulo constant
sigv = tau/sqrt(6);     // square root of the power of the modulo
                        // operators output