function SYS = fir2sys(IR,q)

    // Purpose:
    //
    //  State-space realization with predescribed finite impulse response.
    //
    // Description:
    //
    //  Suppose that we are given a FIR system F(z)=F0+F1/z+F2/z^2+...+FN/z^N.
    //  The routine computes a state-space system SYS with transfer function
    //  F(z), i.e., F(z)=SYS.D+SYS.C*inv(z*eye()-SYS.A)*SYS.B. We assume that
    //  the coefficient matrices F0,...,FN are q x p and that the input IR
    //  satisfies IR=[F0;F1;F2;...;FN].

    // extract dimensions
    [m,p] = size(IR);
    N = m/q;

    // create standard realization for FIR systems
    SYS = syslin("d",[zeros((N-2)*q,q) eye((N-2)*q,(N-2)*q); ...
    zeros(q,(N-1)*q)],IR(q+1:$,:),eye(q,(N-1)*q),IR(1:q,:));

endfunction