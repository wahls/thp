function [s,xx,v] = preamble(SYST,tau,K)

    // Purpose:
    //
    //  Computes a preamble that leaves the modulo operator at the transmitter
    //  invariant.
    //
    // Description:
    //
    //  The input SYST is a state-space realization of the feedback filter T(z).
    //  The inputs tau (a scalar) and K (a natural) denote the modulo constant
    //  and the length of the preamble.

    q = size(SYST.D,1);
    v = zeros(q,K);                 // outputs of the modulo operator
    xx = zeros(q,K);
    xk = zeros(SYST.x0);            // state of the feed-backward filter,
                                    // initial state is zero

    if q~=1 then
        error("TODO");
    end

    for k=1:K do                    // iterate over time slots

        // buffer the part of the current output of the feedback loop which
        // belongs to previous inputs
        t = SYST.C*xk;

        // compute the part of the current output of the feedback loop which
        // belongs to the current input
        for l=1:q do                // iterate over outputs
            if abs(t(l))<tau/2 then
                s(l,k) = sign(rand(1,1,"normal"))*tau/2;
            else
                s(l,k) = -round(t(l)*2/tau)*tau/2;
            end
            x = s(l,k)+t(l);
            xx(l,k) = x;
            v(l,k) = modulo_thp(x,tau); // apply modulo operator
        end

        // update the state of the feedback filter
        xk = SYST.A*xk+SYST.B*v(:,k);

    end

endfunction