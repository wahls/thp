function exa3_base(nch)

    // This is a auxiliary function for the script exa3.sce. It performs
    // the actual simulation. Note: several global variables have to be set
    // before this script can be run (cf. loader.sce and exa3_params.sce).

    // initialize random number generator, salt ensures different seed per SNR
    rand("seed",salt+nch);

    sigsq = Etr/10^(SNR/10)/q;    // noise power (per data stream)

    // create IIR random channel and white noise model
    SYSH = syslin("d", ...
    rand(kappa,kappa,"normal")+%i*rand(kappa,kappa,"normal"), ...
    rand(kappa,p,"normal")+%i*rand(kappa,p,"normal"), ...
    rand(q,kappa,"normal")+%i*rand(q,kappa,"normal"), ...
    rand(q,p,"normal")+%i*rand(q,p,"normal"));
    SYSH.A = 0.97*SYSH.A/max(abs(spec(SYSH.A)));
    SYSPSI = sqrt(sigsq)*syslin("d",[],[],[],eye(q,q));

    // compute the optimal THP for the trunctated channels
    [P_fir100,al_fir100,T_fir100,PI_fir100] = thp_fir(impresp(SYSH,100), ...
    impresp(SYSPSI,1),"successive",sigv,Etr,L);
    [P_fir120,al_fir120,T_fir120,PI_fir120] = thp_fir(impresp(SYSH,120), ...
    impresp(SYSPSI,1),"successive",sigv,Etr,L);
    [P_fir140,al_fir140,T_fir140,PI_fir140] = thp_fir(impresp(SYSH,140), ...
    impresp(SYSPSI,1),"successive",sigv,Etr,L);

    // compute the optimal THP for the untrunctated channel
    [SYSP_iir,al_iir,SYST_iir,PI_iir] = thp_iir(SYSH,SYSPSI,"successive", ...
    sigv,Etr,L);

    // compute bit error rates (we use sparse FIR models for a speed-up)
    ber_fir100 = estimate_ber_qpsk(SYSH,fir2spsys(P_fir100,p), ...
    al_fir100,fir2spsys(T_fir100,q),PI_fir100,L,sqrt(sigsq),tau,Nsymb);
    ber_fir120 = estimate_ber_qpsk(SYSH,fir2spsys(P_fir120,p), ...
    al_fir120,fir2spsys(T_fir120,q),PI_fir120,L,sqrt(sigsq),tau,Nsymb);
    ber_fir140 = estimate_ber_qpsk(SYSH,fir2spsys(P_fir140,p), ...
    al_fir140,fir2spsys(T_fir140,q),PI_fir140,L,sqrt(sigsq),tau,Nsymb);
    ber_iir = estimate_ber_qpsk(SYSH,SYSP_iir,al_iir,SYST_iir, ...
    PI_iir,L,sqrt(sigsq),tau,Nsymb);

    // add results to the data file
    line = sprintf("%e %e %e %e",ber_fir100,ber_fir120, ...
    ber_fir140,ber_iir);
    add_line_to_file(filename,line);

    // print status message
    printf("exa3_base.sci: simulation of channel %5d of %5d at SNR=%3ddB " ...
    +"completed\n",nch,nruns,SNR);

endfunction