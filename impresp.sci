function T = impresp(SYS,K)

    // Purpose:
    //
    //  Computes the first K taps of the impulse reponse of SYS.
    //
    // Description:
    //
    //  We assume that SYS is a discrete-time state-space system (a syslin
    //  list) and that K is a natural. Let T(z)=T0+T1/z+T2/z^2+T3/z^3+... denote
    //  the transfer function of SYS. The routine computes the block column
    //  vector T=[T0;T1;T2;...;TK-1].

    [q,p] = size(SYS);
    if SYS.A==[] then
        SYS.A = 0;
        SYS.B = zeros(1,p);
        SYS.C = zeros(q,1);
    end
    T = zeros(K*q,p);
    T(1:q,:) = SYS.D;
    if K>=2 then
        T(q+1:2*q,:) = SYS.C*SYS.B;
        for k=3:K do
            T((k-1)*q+1:k*q,:)= SYS.C*SYS.A^(k-2)*SYS.B;
        end
    end

endfunction