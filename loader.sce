mode(-1);

// This is a auxiliary routine which checks whether the necessary toolboxes are
// available and loads various auxiliary routines which are required for the
// execution of the main scripts.

// check whether the routine ldl_gs is available
try
    ldl_gs(sparse([0 0;1 0]),[1 0;2 2],1,1);
catch
    printf("\nThe Factorization of Structured Matrices Toolbox hasn''t been" ...
    +"\nloaded. Please see the included README.txt file for more\n" ...
    +"information on how to install and load the toolbox!\n\n");
    error("fsmtbx is missing");
end

// check whether the routines dh2norm and add_delay are available
try
    dh2norm(syslin("d",0.5,1,1,2));
    add_delay(syslin("d",0.5,1,1,2),1);
catch
    printf("\nThe Linear System Inversion Toolbox hasn''t been\n" ...
    +"loaded. Please see the included README.txt file for more\n" ...
    +"information on how to install and load the toolbox!\n\n");
    error("lsitbx is missing");
end

// load auxiliary routines
prot = funcprot();
funcprot(0);
exec add_line_to_file.sci;
exec cplxdsimul.sci;
exec encode_thp.sci;
exec estimate_ber_qpsk.sci;
exec estimate_ber_16qam.sci;
exec exa1_base.sci;
exec exa2a_base.sci;
exec exa2b_base.sci;
exec exa3_base.sci;
exec fir2spsys.sci;
exec fir2sys.sci;
exec fwdbwdsubst.sci;
exec generator.sci;
exec impresp.sci;
exec modulo_thp.sci;
exec num_lines_in_a_file.sci;
exec preamble.sci;
exec tapk.sci;
exec thp_fir.sci;
exec thp_iir.sci;
exec thp_naive.sci;
exec uk.sci;
exec uk_pisucc.sci;
funcprot(prot);