function Pv = encode_thp(SYSH,SYSP,SYST,PI,L,tau,s)

    // Purpose:
    //
    //  Computes the output of the Tomlinson-Harashima Precoder.
    //
    // Description:
    //
    //  The input parameters SYSH, SYSP and SYST are state-space realizations
    //  of the channel H(z), the feedforward filter P(z) and the feedback filter
    //  T(z). The inputs PI (a matrix) and tau (a scalar) denote the permutation
    //  matrix and the modulo constant. The input s=[s0 s1 s2 ... sN] is a
    //  stacked matrix which contains the to be transmitted data vectors. The
    //  output parameter Pv=[pv0 pv1 pv2 ... pvN] also is a stacked matrix which
    //  contains the according outputs of the feed-forward filter P(z).

    [q,K] = size(s);
    s = [PI*s zeros(q,L)];          // zero-padded & permutated data vectors
    v = zeros(q,K+L);               // outputs of the modulo operator
    xk = zeros(SYST.x0);            // state of the feedback filter, initial
                                    // state is zero

    for k=1:K+L do                  // iterate over time slots

        // buffer the part of the current output of the feedback loop which
        // belongs to previous inputs
        t = SYST.C*xk;

        // compute the part of the current output of the feedback loop which
        // belongs to the current input
        for l=1:q do                  // iterate over the data streams
            x = s(l,k)+t(l);
            for r=1:l-1 do
                x = x+SYST.D(l,r)*v(r,k);
            end
            v(l,k) = modulo_thp(x,tau); // apply modulo operator
        end

        // update the state of the feedback filter
        xk = SYST.A*xk+SYST.B*v(:,k);

    end

    // apply the feedforward filter
    Pv = cplxdsimul(SYSP,v);

endfunction