function [P,al,T,PI] = thp_fir(H,PSI,PI,sigv,Etr,L)

    // Purpose:
    //
    //  Fast computation of the optimal Tomlinson-Harashima precoder for FIR
    //  channels. Channel and filters are specified via impulse responses.
    //
    // Description:
    //
    //  We consider a FIR channel H(z)=H0+H1/z+H2/z^2+...+HN/z^N and a FIR noise
    //  model PSI(z)=PSI0+PSI1/z+PSI2/z^2+...+PSIM/z^M. The input
    //  H=[H0;H1;H2;...;HN] and PSI=[PSI0;PSI1;PSI2;...;PSIM] then should be
    //  block vectors which specify the impulse responses. The other parameters
    //  are the permutation matrix PI, the square root of the power of the
    //  signal v sigv, the transmission power Etr and the latency time L. The
    //  output parameters P and T contain the impulse responses of the optimal
    //  feedforward and feedback filters, where we use the same format as for H
    //  and PSI. The scalar al specifies the optimal scalar gain at the
    //  receiver. Note that it is possible to replace the permutation matrix PI
    //  with the string "successive". In that case a additional successive
    //  optimization of the permutation matrix is carried out and the result is
    //  returned in the fourth output parameter PI.

    // extract channel dimensions
    p = size(H,2);
    q = size(PSI,2);
    N = size(H,1)/q;

    // compute the noise power
    sqnrm_SYSPSI = trace(PSI'*PSI);

    // find the uk and, if requested, optimize the permutation matrix
    // successively
    if PI=="successive" then
        [U,PI] = uk_pisucc(fir2spsys(H,q),sqnrm_SYSPSI,sigv,Etr,L);
    else
        U = uk(fir2spsys(H,q),sqnrm_SYSPSI,PI,sigv,Etr,L);
    end

    // compute the scalar gain and feedforward filter
    al = sigv*norm(U,"fro")/sqrt(Etr);
    P = U/al;

    // compute (parts of) the convolution of channel and scaled feedforward
    // filter
    NN = zeros(N*q,q);
    for k=L:L+N do
        for j=0:min(N,k) do
            NN((k-L)*q+1:(k-L+1)*q,:) = tapk(NN,q,k-L)+tapk(H,q,j)*tapk(U,p,k-j);
        end
    end

    // set the first tap of the feedback filter
    T0 = -PI*tapk(NN,q,0);
    for k=1:q do
        for j=k:q do
            T0(k,j) = 0;
        end
    end

    // set the remaining taps of the feedback filter
    T = zeros(NN);
    T(1:q,:) = T0;
    for k=1:N do
        T(k*q+1:(k+1)*q,:) = -PI*tapk(NN,q,k);
    end

endfunction