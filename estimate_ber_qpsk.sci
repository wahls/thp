function [ber,s,y,shat] = estimate_ber_qpsk(SYSH,SYSP,al,SYST,PI,L,sig_eta, ...
    tau,Nsymb)

    // Purpose:
    //
    //  Performs a simulation in order to estimate the uncoded bit error rate of
    //  a given Tomlinson-Harashima precoder for QPSK modulated data vectors.
    //
    // Description:
    //
    //  The input parameters SYSH, SYSP and SYST are state-space realizations of
    //  the channel H(z), the precoder P(z) and the feedback filter T(z). The
    //  parameters al (a scalar), PI (a matrix), L (a natural) and tau (a
    //  scalar) are the scalar gain at the receiver, the permutation matrix, the
    //  latency time and the modulo constant. The input parameter sig_eta (a
    //  scalar) specifies the square root of the noise power per data stream and
    //  Nsymb (a natural) is the number of data vectors to be transmitted in the
    //  simulation. The output parameter ber (a scalar) contains the bit error
    //  rate. The optional outputs s (a qxNsymb matrix), y (a qx(L+Nsymb)
    //  matrix) and shat (a qx(L+Nsymb) matrix) contain the data signals, the
    //  received signals and the estimated data signals, respectively.

    // extract channel dimensions
    [q,p] = size(SYSH);

    // create random QPSK data vectors with avg. power one (per data stream)
    s = (sign(1-2*rand(q,Nsymb))+%i*sign(1-2*rand(q,Nsymb)))/sqrt(2);

    // compute the according output of the Tomlinson-Harashima precoder
    Pv = encode_thp(SYSH,SYSP,SYST,PI,L,tau,s);

    // create noise vectors
    eta = sig_eta*(rand(q,Nsymb+L,"normal")+%i*rand(q,Nsymb+L,"normal")) ...
    /sqrt(2);

    // compute the received vectors
    y = cplxdsimul(SYSH,Pv)+eta;

    // apply the scalar gain and the modulo operator to the received signals
    shat = modulo_thp(al*y,tau);

    // compute the bit error rate
    ber = (sum(sum(sign(real(s))~=sign(real(shat(:,L+1:$))))) ...
    +sum(sum(sign(imag(s))~=sign(imag(shat(:,L+1:$))))))/Nsymb/2/q;

endfunction