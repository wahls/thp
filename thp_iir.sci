function [SYSP,al,SYST,PI] = thp_iir(SYSH,SYSPSI,PI,sigv,Etr,L)

    // Purpose:
    //
    //  Fast computation of the optimal Tomlinson-Harashima precoder for IIR
    //  channels. Channels and filters are specified via state-space systems.
    //
    // Description:
    //
    //  The inputs SYSH and SYSPSI should be state-space realizations of the
    //  channel and the noise model, respectively. The other parameters are the
    //  permutation matrix PI, the square root of the power of the signal v
    //  sigv, the transmission power Etr and the latency time L. The output
    //  parameters SYSP and SYST are state-space realizations of the optimal
    //  feedforward and feedback filters. The scalar al specifies the optimal
    //  scalar gain at the receiver. Note that it is possible to replace the
    //  permutation matrix PI with the string "successive". In that case a
    //  additional successive optimization of the permutation matrix is carried
    //  out and the result is returned in the fourth output parameter PI.

    // extract dimensions of the channels state-space realization
    [q,p,n] = size(SYSH);

    // compute the noise power
    sqnrm_SYSPSI = dh2norm(SYSPSI)^2;

    // compute (parts of) the channel's impulse response and save some
    // intermediate results for later use
    H = SYSH.D;
    E = SYSH.B;
    for k=1:L do
        H = [H;SYSH.C*E($-n+1:$,:)];
        E = [E;SYSH.A*E($-n+1:$,:)];
    end

    // find the uk and, if requested, optimize the permutation matrix
    // successively
    if PI=="successive" then
        [U,PI] = uk_pisucc(SYSH,sqnrm_SYSPSI,sigv,Etr,L);
    else
        U = uk(SYSH,sqnrm_SYSPSI,PI,sigv,Etr,L);
    end

    // compute the scalar gain and the feedforward filter
    al = sigv*norm(U,"fro")/sqrt(Etr);
    SYSP = fir2sys(U/al,p);

    // We use an extended version of Lemma 5 in the paper "Efficient Computation
    // of the Realizable MIMO DFE" by Wahls & Boche, Proc. IEEE ICASSP 2010
    // (Dallas, TX) in order to find a state-space realization for the strictly
    // causal part of the feedback filter (up to a delay and permutation); the
    // advantage of this approach is that the McMillan degree of the feedback
    // filter equals that of the channel. The exact result will be contained in
    // the first authors doctoral thesis.
    X = zeros(n,q);
    for k=1:L do
        X = X+tapk(E,n,k-1)*tapk(U,p,L-k);
    end
    SYST = -syslin("d",SYSH.A,SYSH.A*X+SYSH.B*tapk(U,p,L),PI*SYSH.C, ...
    PI*(SYSH.C*X+SYSH.D*tapk(U,p,L)));
    for i=1:q do
        for j=i:q do
            SYST.D(i,j) = 0;
        end
    end

endfunction