function [SYSP,al,SYST,U] = thp_naive(SYSH,SYSPSI,PI,sigv,Etr,L)

    // Purpose:
    //
    //  Naive computation of the optimal Tomlinson-Harashima precoder for IIR
    //  channels. Channels and filters are specified via state-space systems.
    //
    // Description:
    //
    //  This routine computes the optimal Tomlinson-Harashima precoder in a 
    //  straight-forward way. The structure of the problem is NOT
    //  exploited. The routine is intended as a reference implementation. The
    //  input parameters are SYSH (a state-space realization of the channel
    //  H(z)), SYSPSI (a state-space realization of the noise model), PI (the
    //  permutation matrix), sigv (square root of the power of the signal v),
    //  Etr (the transmission power) and L (the latency time). The output
    //  parameters are SYSP (a state-space realization of the optimal
    //  feedforward filter), al (the optimal scalar gain at the receiver) and
    //  SYST (a state-space realization of the optimal feedback filter). The
    //  output parameter U is a matrix whose columns are the uk which have to be
    //  computed during the determination of the other output parameters.

    // extract channel dimensions
    [q,p] = size(SYSH);

    // construct M
    H = impresp(PI*SYSH,L+1);
    M = [H zeros((L+1)*q,L*p)];
    for k=2:L+1 do
        M((k-1)*q+1:$,(k-1)*p+1:k*p) = H(1:$-(k-1)*q,:);
    end

    // compute the uk
    U = zeros((L+1)*p,q);
    for k=1:q do
        e = zeros((L+1)*q,1);
        e(L*q+k) = 1;
        uk = inv(M'*[eye(L*q+k,L*q+k) zeros(L*q+k,q-k);zeros(q-k,L*q+k) ...
        zeros(q-k,q-k)]*M+dh2norm(SYSPSI)^2/Etr*eye())*M'*e;
        U(:,k) = uk;
    end

    // compute the feedforward filter and the scalar gain
    SYSPtil = fir2sys(U,p);
    al = sigv*dh2norm(SYSPtil)/sqrt(Etr);
    SYSP = SYSPtil/al;

    // compute the first tap of the feedback filter
    HPtil = impresp(SYSH*SYSPtil,L+1);
    T0 = -PI*tapk(HPtil,q,L);
    for i=1:q do
        for j=i:q do
            T0(i,j) = 0;
        end
    end

    // compute the strictly causal part of the feedback filter and integrate it
    // with the first tap
    SYST = -PI*SYSH*SYSPtil;
    SYST = add_delay(syslin("d",SYST.A,SYST.B,SYST.C*SYST.A^(L+1), ...
    SYST.C*SYST.A^L*SYST.B),1)+T0;

endfunction