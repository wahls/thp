function SYS = fir2spsys(IR,q)

    // Purpose:
    //
    //  State-space realization with predescribed finite impulse response and
    //  sparse SYS.A and SYS.C matrices.
    //
    // Description:
    //
    //  Suppose that we are given a FIR system F(z)=F0+F1/z+F2/z^2+...+FN/z^N.
    //  The routine computes a state-space system SYS with transfer function
    //  F(z), i.e., F(z)=SYS.D+SYS.C*inv(z*eye()-SYS.A)*SYS.B. We assume that
    //  the coefficient matrices F0,...,FN are q x p and that the input IR
    //  satisfies IR=[F0;F1;F2;...;FN]. Note: SYS.A and SYS.C will be sparse.

    // extract dimensions
    [m,p] = size(IR);
    N = m/q;

    // Construct standard realization for FIR systems. We make the A and C
    // matrices sparse which make evaluation of the system much faster if N
    // is large. Note that syslin does not accept sparse matrices. Hence, we
    // have to modify an existing system.
    SYS = syslin("d",0,0,0,0);
    SYS.A = [spzeros((N-2)*q,q) speye((N-2)*q,(N-2)*q);spzeros(q,(N-1)*q)];
    SYS.B = IR(q+1:$,:);
    SYS.C = speye(q,(N-1)*q);
    SYS.D = IR(1:q,:);
    SYS.X0 = zeros((N-1)*q,1);

endfunction