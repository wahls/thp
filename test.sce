mode(-1);

// This test script compares the results of the fast algorithms  with a naive
// implementation of the optimal Tomlinson-Harashima precoder. The script either
// terminates peacefully if all tests are passed or aborts with an error
// otherwise.

// load auxiliary routines
exec loader.sce;

// fix the random number generator => avoids rare random failures
rand("seed",123);

// set some parameters
tol = 1e-8;             // tolerance during the tests
q = 2;                  // no. of receiving antennae
p = 3;                  // no. of transmitting antennae
N = 7;                  // length of the channel (we use a FIR channel model)
PI = eye(q,q);          // default permutation matrix
tau = 1;                // modulo constant
sigv = tau/sqrt(6);     // power of the output of the modulo operator
Etr = 1;                // transmit power
L = 4;                  // latency time

// generate channel and noise noise model
H = rand(N*q,p,"normal")+%i*rand(N*q,p,"normal");
SYSH = fir2sys(H,q);
SYSPSI = syslin("d",[],[],[],eye(q,q));
sqnrm_SYSPSI = dh2norm(SYSPSI)^2;

// compute optimal THP with the naive implementation (=reference)
[SYSP1,al1,SYST1,U1] = thp_naive(SYSH,SYSPSI,PI,sigv,Etr,L);

// test the recursive computation of the uk without successive optimization of
// the permutation matrix
U2 = uk(SYSH,sqnrm_SYSPSI,PI,sigv,Etr,L);
if norm(U2-U1)>tol then error("test failed"), end;

// test the fast computation of the optimal THP in the FIR case without
// successive optimization of the permutation matrix
[P2,al2,T2] = thp_fir(H,impresp(SYSPSI,1),PI,sigv,Etr,L);
if dh2norm(fir2sys(P2,p)-SYSP1)>tol then error("test failed"), end;
if abs(al2-al1)>tol then error("test failed"), end;
if dh2norm(fir2sys(T2,q)-SYST1)>tol then error("test failed"), end;

// test the fast computation of the optimal THP in the IIR case without
// successive optimization of the permutation matrix
[SYSP3,al3,SYST3] = thp_iir(SYSH,SYSPSI,PI,sigv,Etr,L);
if dh2norm(SYSP3-SYSP1)>tol then error("test failed"), end;
if abs(al3-al1)>tol then error("test failed"), end;
if dh2norm(SYST3-SYST1)>tol then error("test failed"), end;

// test the recursive computation of the the uk with successive optimization of
// the permutation matrix (only tests consistency with the routine uk)
[U4,PIsubopt4] = uk_pisucc(SYSH,sqnrm_SYSPSI,sigv,Etr,L);
U2subopt = uk(SYSH,sqnrm_SYSPSI,PIsubopt4,sigv,Etr,L);
if norm(U4-U2subopt)>tol then error("test failed"), end;
