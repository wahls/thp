
// This is a auxiliary script for the script exa2.sce. Its purpose is to set
// various simulation parameters.

q = 3;                  // no. of channel outputs; note: q~=3 requires some
                        // changes to exa2a.sce and exa2b.sce (the according
                        // set of permutation matrices has to be specified)
p = 4;                  // no. of channel inputs
N = 1;                  // degree of the channel (FIR case)
n = 5;                  // McMillan degree of the channels (IIR case)
L = 5;                  // latency time
SNRs = -15:8;           // signal to noise ratios (SNRs)
Nsymb = 1000;           // no. of transmitted data symbols per SNR and channel
Nchan = 100000;         // no. of random channels per SNR
Etr = 1.2;              // transmit power
tau = 2*sqrt(2);        // modulo constant
sigv = tau/sqrt(6);     // square root of the power of the modulo
                        // operators output