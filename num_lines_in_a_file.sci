function num = num_lines_in_a_file(filename)
    
    // Purpose:
    //
    //  Returns the number of lines in a file.
    //
    // Description:
    //
    //  Returns the number of lines in the file specified by the string filename
    //  or zero if that file cannot be opened.
    
    try
        txt = mgetl(filename);
        num = size(txt,1);
    catch
        num = 0;
    end
    
endfunction
