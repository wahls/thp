function T = tapk(IR,q,k)

    // Purpose:
    //
    //  Extracts single coefficients of a impulse response in stacked matrix
    //  form.
    //
    // Description:
    //
    //  Consider the q x p FIR system F(z)=F0+F1/z+F2/z^2+...+FN/z^N and assume
    //  that input IR is the N*q x p matrix IR=[F0;F1;F2;...;FN]. The routine
    //  returns T=Fk where k is any integer.

    [m,p] = size(IR);
    K = m/q;
    m1 = k*q+1;
    m2 = (k+1)*q;
    if (k<0)|(m2>m) then  // return zero if the index is negative or too large
        T = zeros(q,p);
    else                  // extract the coefficient from the matrix
        T = IR(m1:m2,:);
    end

endfunction