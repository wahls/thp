function add_line_to_file(filename,line)
    
    // Purpose:
    //
    //  Appends a line to a file.
    //
    // Description:
    //
    //  Appends the content of the string line to the file specfied by string
    //  filename. The string line need not contain a newline.
    
    [fd,err] = mopen(filename,"a");
    if err~=0 then
        error();
    end
    mputl(line,fd);
    mclose(fd);

endfunction
