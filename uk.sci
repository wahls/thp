function U = uk(SYSH,sqnrm_SYSPSI,PI,sigv,Etr,L)

    // Purpose:
    //
    //  Fast computation of the uk which are required during the computation of
    //  the optimal Tomlinson-Harashima precoder.
    //
    // Description:
    //
    //  The input parameters are SYSH (a state-space realization of the channel
    //  H(z)), sqnrm_SYSPSI (squared norm of the noise model), PI (permutation
    //  matrix), sigv (square root of the power of the signal v), Etr (transmit
    //  power) and L (the latency time). The output parameter U is a matrix
    //  which contains the impulse response of the optimal precoder (up to a
    //  scaling) Ptilde(z).

    // extract channel dimensions
    [q,p] = size(SYSH);

    // Note: we have PHI0 = [PHI0red zeros();zeros() sqnrm_SYSPSI/Etr*eye(q,q)].
    // Hence, it suffices to factor PHI0red=Rred*Rred' in order to find the
    // Cholesky factorization of PHI0.

    // find a generator for PHI0red 
    [F,G] = generator(SYSH,sqnrm_SYSPSI,Etr,L);

    // fast computation of the Cholesky factorization of PHI0red 
    [Rred,d] = ldl_gs(F,G',p,p+q);

    // extend to an Cholesky factorization of PHI0
    R = [Rred zeros(L*p,p);zeros(p,L*p) sqrt(sqnrm_SYSPSI/Etr)*eye(p,p)];

    // precompute V=[v1,...,vq]
    V = zeros((L+1)*p,q);
    tmp = impresp(PI*SYSH,L+1);
    for k=1:L+1 do
        V((k-1)*p+1:k*p,:) = tapk(tmp,q,L+1-k)';
    end

    // perform the recursion
    U = zeros((L+1)*p,1);
    W = zeros((L+1)*p,1);
    for k=1:q do
        x = fwdbwdsubst(R,V(:,k));
        tmp = zeros((L+1)*p,1);
        for j=1:k-1 do
            tmp = tmp+(W(:,j)'*V(:,k))/(1+V(:,j)'*W(:,j))*W(:,j);
        end
        W(:,k) = x-tmp;
        U(:,k) = (1-(W(:,k)'*V(:,k))/(1+V(:,k)'*W(:,k)))*W(:,k);
    end

endfunction