function exa2b_base(nch)

    // This is a auxiliary function for the script exa2.sce. It performs
    // the actual simulation. Note: several global variables have to be set
    // before this script can be run (cf. loader.sce and exa2_params.sce).

    // initialize random number generator, salt ensures different seed per SNR
    rand("seed",salt+nch);

    // prepare the set of all permutation matrices for exhaustive search of the
    // optimal one; we only prepare the case q=3
    PIs = zeros(q,q,factorial(q));
    if q==3 then
        PIs(:,:,1) = [1 0 0; 0 1 0; 0 0 1];
        PIs(:,:,2) = [1 0 0; 0 0 1; 0 1 0];
        PIs(:,:,3) = [0 1 0; 1 0 0; 0 0 1];
        PIs(:,:,4) = [0 0 1; 0 1 0; 1 0 0];
        PIs(:,:,5) = [0 1 0; 0 0 1; 1 0 0];
        PIs(:,:,6) = [0 0 1; 1 0 0; 0 1 0];
    else
        error("TODO");
    end

    // noise power (per data stream)
    sigsq = Etr/10^(SNR/10)/q;

    // create FIR random channel and white noise model
    H = kron(diag(exp(-0.5*(0:N))),eye(q,q))*(rand(q*(N+1),p,"normal") ...
    +%i*rand(q*(N+1),p,"normal"));
    PSI = sqrt(sigsq/q)*eye(q,q);

    // compute the optimal THP for the permutation matrix PI=identity matrix
    [P_noopt,al_noopt,T_noopt] = thp_fir(H,PSI,eye(q,q),sigv,Etr,L);

    // compute the optimal THP for the successively optimized permutation matrix
    [P_succ,al_succ,T_succ,PI_succ] = thp_fir(H,PSI,"successive",sigv,Etr,L);

    // compute the optimal THP for the optimal permutation matrix found by
    // exhaustive search
    mse_opt = %inf;
    for k=1:factorial(q) do
        PI = PIs(:,:,k);
        [P,al,T] = thp_fir(H,PSI,PI,sigv,Etr,L);
        mse = sigv^2*dh2norm(add_delay(PI'*(eye()-fir2sys(T,q)),L)- ...
        al*fir2sys(H,q)*fir2sys(P,p))^2+al^2*dh2norm(fir2sys(PSI,q))^2;
        if real(mse)<mse_opt then
            mse_opt = real(mse);
            P_opt = P;
            al_opt = al;
            T_opt = T;
            PI_opt = PI;
        end
    end

    // compute bit error rates
    ber_noopt = estimate_ber_qpsk(fir2sys(H,q),fir2sys(P_noopt,p), ...
    al_noopt,fir2sys(T_noopt,q),eye(q,q),L,sqrt(sigsq),tau,Nsymb);
    ber_succ = estimate_ber_qpsk(fir2sys(H,q),fir2sys(P_succ,p), ...
    al_succ,fir2sys(T_succ,q),PI_succ,L,sqrt(sigsq),tau,Nsymb);
    ber_opt = estimate_ber_qpsk(fir2sys(H,q),fir2sys(P_opt,p),al_opt, ...
    fir2sys(T_opt,q),PI_opt,L,sqrt(sigsq),tau,Nsymb);

    // add results to the data file
    line = sprintf("%e %e %e",ber_noopt,ber_succ,ber_opt);
    add_line_to_file(filename,line);

    // print status message
    printf("exa2b_base.sci: simulation of channel %5d of %5d at SNR=%3ddB "...
    +"completed\n",nch,nruns,SNR);

endfunction