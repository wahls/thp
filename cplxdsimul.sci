function Y = cplxdsimul(SYS,U)

    // Purpose:
    //
    //  Simulation of a discrete-time state-space system.
    //
    // Description:
    //
    //  This is a replacement for Scilabs dsimul routine which only handles
    //  real non-sparse systems. SYS is a q x p discrete-time state-space
    //  realization and U=[u(0) u(1) ... u(N-1)] is a N x p vector. Then, the
    //  output is the N x q vector Y=[y(0) y(1) ... y(N-1)] specified by the
    //  recursion x(k+1)=SYS.A*x(k)+SYS.B*u(k), x(0)=zeros(),
    //  y(k)=SYS.C*x(k)+SYS.D*u(k), where k=0,1,...,N.


    [q,p,n] = size(SYS);
    [m,l] = size(U);
    X = [zeros(n,1) SYS.B*U(:,1:l-1)];
    for k = 1:l-1
        X(:,k+1) = X(:,k+1)+SYS.A*X(:,k);
    end
    Y = SYS.C*X+SYS.D*U;

endfunction
