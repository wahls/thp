function x = fwdbwdsubst(R,v)

    // Purpose:
    //
    //  Forward-backward substitution.
    //
    // Decription:
    //
    //  The routine solves the linear equation system R*R'*x=v for x. We assume
    //  that R is lower triangular and invertible.

    n = size(R,1);
    xtil = zeros((L+1)*p,1);
    xtil(1) = v(1)/R(1,1);
    for k=2:n do
        xtil(k) = (v(k) - R(k,1:k-1)*xtil(1:k-1))/R(k,k);
    end
    x = zeros(n,1);
    x(n) = xtil(n)/R(n,n)';
    for k=n-1:-1:1 do
        x(k) = (xtil(k) - R(k+1:n,k)'*x(k+1:n))/R(k,k)';
    end

endfunction