function exa1_base(nch)

    // This is a auxiliary function for the script exa1.sce. It performs
    // the actual simulation. Note: several global variables have to be set
    // before this script can be run (cf. loader.sce and exa1_params.sce).

    // initialize random number generator, salt ensures different seed per SNR
    rand("seed",salt+nch);

    // noise power (per data stream)
    sigsq = Etr/10^(SNR/10)/q;

    // create IIR random channel and white noise model
    SYSH = syslin("d", ...
    rand(kappa,kappa,"normal")+%i*rand(kappa,kappa,"normal"), ...
    rand(kappa,p,"normal")+%i*rand(kappa,p,"normal"), ...
    rand(q,kappa,"normal")+%i*rand(q,kappa,"normal"), ...
    rand(q,p,"normal")+%i*rand(q,p,"normal"));
    SYSH.A = 0.97*SYSH.A/max(abs(spec(SYSH.A)));
    SYSPSI = sqrt(sigsq)*syslin("d",[],[],[],eye(q,q));

    // compute the optimal THP
    [SYSP,al_opt,SYST,PI] = thp_iir(SYSH,SYSPSI,"successive", ...
    sigv,Etr,L);

    // measure scaling factor of the non-linear mismatch correction
    [s,xx,v] = preamble(SYST,tau,Npreamble);
    Pv = encode_thp(SYSH,SYSP,SYST,PI,L,tau,s);
    y = cplxdsimul(SYSH,Pv)+sqrt(sigsq/q/2)*(rand(q,Npreamble+L, ...
    "normal")+%i*rand(q,Npreamble+L,"normal"));
    al_scl_meas = norm(s(:,1:$-L),"fro")/norm(y(:,L+1:$),"fro");

    // analytic derivation of the non-linear mismatch correction
    al_scl = sqrt(dh2norm(eye(q,q)-SYST)^2/(dh2norm(SYSH*SYSP)^2+ ...
    1/sigv^2*dh2norm(SYSPSI)^2));

    // compute the relative differences between al_opt and al_scl and
    // al_scl and al_scl_meas
    rel_diff_1 = abs((al_opt-al_scl)/al_opt);
    rel_diff_2 = abs((al_scl-al_scl_meas)/al_scl);

    // compute bit error rates
    ber_alopt = estimate_ber_16qam(SYSH,SYSP,al_opt,SYST, ...
    PI,L,sqrt(sigsq),tau,Nsymb);
    ber_alscl = estimate_ber_16qam(SYSH,SYSP,al_scl,SYST, ...
    PI,L,sqrt(sigsq),tau,Nsymb);
    ber_alsclmeas = estimate_ber_16qam(SYSH,SYSP, ...
    al_scl_meas,SYST,PI,L,sqrt(sigsq),tau,Nsymb);

    // add results to the data file
    line = sprintf("%e %e %e %e %e",rel_diff_1,rel_diff_2, ...
    ber_alopt,ber_alscl,ber_alsclmeas);
    add_line_to_file(filename,line);

    // print status message
    printf("exa1_base.sci: simulation of channel %5d of %5d at SNR=%3ddB " ...
    +"completed\n",nch,nruns,SNR);

endfunction