function [U,PIsucc] = uk_pisucc(SYSH,sqnrm_SYSPSI,sigv,Etr,L)

    // Purpose:
    //
    //  Joint successive optimization of the precoding matrix PI and fast
    //  computation of the uk which are required during the computation of the
    //  optimal Tomlinson-Harashima precoder
    //
    // Description:
    //
    //  The input parameters are SYSH (a state-space realization of the
    //  channel H(z)), sqnrm_SYSPSI (squared norm of the noise model),sigv
    //  (square root of the power of the signal v), Etr (transmit power) and L
    //  (the latency time). The output parameter PIsucc is the permutation
    //  matrix which has been found as a result of the successive optimization
    //  procedure. The output parameter U is a matrix which contains the impulse
    //  response of the optimal precoder (up to a scaling) Ptilde(z). Note that
    //  this is the precoder that corresponds to the successively found 
    //  permutation matrix PIsucc.

    // extract channel dimensions
    [q,p] = size(SYSH);

    // Note: we have PHI0 = [PHI0red zeros();zeros() sqnrm_SYSPSI/Etr*eye(q,q)].
    // Hence, it suffices to factor PHI0red=Rred*Rred' in order to find the
    // Cholesky factorization of PHI0.

    // find a generator for PHI0red 
    [F,G] = generator(SYSH,sqnrm_SYSPSI,Etr,L);

    // fast computation of the Cholesky factorization of PHI0red 
    [Rred,d] = ldl_gs(F,G',p,p+q);

    // extend to an Cholesky factorization of PHI0
    R = [Rred zeros(L*p,p);zeros(p,L*p) sqrt(sqnrm_SYSPSI/Etr)*eye(p,p)];

    // construct V=[v1,...,vq]
    V = zeros((L+1)*p,q);
    for k=1:L+1 do
        V((k-1)*p+1:k*p,:) = tapk(H,q,L+1-k)';
    end

    // pre-buffer the solutions X=[x1,...,xq] to PHI0*xk=vk
    X = zeros((L+1)*p,q);
    for k=1:q do
        X(:,k) = fwdbwdsubst(R,V(:,k));
    end

    // initialize the permutation order
    pi = 1:q;

    // initialize buffers for the uk, wk, gammal
    U = zeros((L+1)*p,1);
    W = zeros((L+1)*p,1);
    GAM = zeros((L+1)*p,q);

    // iterate over the data streams
    for k=1:q do

        // reset maximum MSE
        ximax = -1;

        // iterate over still unassigned data streams
        for l=k:q do

            // compute w
            w = X(:,pi(l))-GAM(:,pi(l));

            // compute u
            u = (1-(w'*V(:,pi(l)))/(1+V(:,pi(l))'*w))*w;

            // compute the corresponding MSE
            xi = sigv^2*(1-V(:,pi(l))'*u);

            // if the achieved MSE is larger than the previous ones, buffer
            // results
            if real(xi)>ximax then
                ximax = real(xi);
                lmax = l;
                W(:,k) = w;
                U(:,k) = u;
            end

        end

        // modify permutation order
        tmp = pi(k);
        pi(k) = pi(lmax);
        pi(lmax) = tmp;

        // update buffers that keep track of the sums
        for l=k+1:q do
            GAM(:,pi(l)) = GAM(:,pi(l)) + (W(:,k)'*V(:,pi(l))) ...
            /(1+V(:,pi(k))'*W(:,k))*W(:,k);
        end

    end

    // convert the permutation order into a permutation matrix
    PIsucc = zeros(q,q);
    for k=1:q do
        PIsucc(k,pi(k)) = 1;
    end

endfunction