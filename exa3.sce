mode(-1);

// This script recreates the figure shown in the third example of the paper.
// Multiples cores are exploited in Linux enviroments. The script can be stopped
// and resumed because intermediate results are stored in the data files
// exa3_data_SNRxxx.txt. Remove these if a new simulation run is desired, e.g.,
// because parameters have changed.

// load auxiliary routines
exec loader.sce;

// load simulation parameters
exec exa3_params.sce;

// create a salt value (here, this means a offset for random seeds)
rand("seed",getdate("s"));
salt = round(1e9*rand(1,1,"uniform"));

// start runtime measurement
tic();

// iterate over SNRs (defined in exa1_params.sce)
for SNR=SNRs do

    // filename of the current data file (used by exa3_base())
    filename = sprintf("exa3_data_SNR%d.txt",SNR);

    // determines how many data sets have to be generated until
    // we have enough data
    nruns = max(0,Nchan-num_lines_in_a_file(filename));

    // inform user about already found data sets, if there are any
    if nruns<Nchan then
        printf("exa3.sce: data file ""%s"" already contains %d data sets, " ...
        +"%d remain\n",filename,Nchan-nruns,nruns);
    end

    // generate the missing data sets, exploit multicore if possible (Linux
    // only); note: exa3_base() uses serveral global variables defined by the
    // previous call to exa3_params.sce
    if nruns>0 then
        parallel_run(1:nruns,exa3_base);
    end

    // increase salt for next SNR because exa3_base() gets the same seeds from
    // parallel_run
    salt = salt + nruns;

end

// stop runtime measurement and show runtime
t = toc();
printf("\nexa3.sce: simulation has finished after %g seconds\n\n",t);

// plot results
exec exa3_plot.sce;

