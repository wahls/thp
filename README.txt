DESCRIPTION

This archive contains the accompanying source code for the paper "Realizable
Spatio-Temporal Tomlinson-Harashima Precoders: Theory and Fast Computation" by
S. Wahls and H. Boche.

The source code in this archive has been written by Sander Wahls as an employee
of the Chair of Theoretical Information Technology at the Technische Universität
München in Munich, Germany. The code is available under the terms of the Apache
License, Version 2.0. Please see the accompanying file APACHE_LICENSE_2.0.txt or
visit http://www.apache.org/licenses/LICENSE-2.0 for more information.

PLATFORM

We performed our simulations on a 64 bit Linux system. However, the software
should also work on all other platforms supported by Scilab 5.3.3 (e.g., 32 bit
Linux, Windows, etc).

ENVIRONMENT

The examples have been implemented using Scilab, a free platform for numerical
computation. Scilab is available on many platforms including Windows, Linux and
MacOS X. We have used the official 5.3.3 64 bit Linux release of Scilab which
can be obtained in the internet at http://www.scilab.org. Additionally, we have
used two toolboxes which provide additionally functionality to Scilab. These
toolboxes were the "Linear System Inversion Toolbox" (version 1.0.1) as well as
the "Factorization of Structured Matrices Toolbox" (version 0.1). We refer to
the internet addresses http://lsitbx.origo.ethz.ch and
http://fsmtbx.origo.ethz.ch for further details.

MAJOR COMPONENT DESCRIPTION

exa1.sce - Scilab script which creates the plots for the first numerical example
exa2.sce - Scilab script which creates the plot for the second numerical example
exa3.sce - Scilab script which creates the plot for the third numerical example

DETAILED SET-UP INSTRUCTIONS

1. Download and install Scilab 5.3.3 from http://www.scilab.org (later versions
  should also work).

2. Install the "Linear System Inversion Toolbox" and the "Factorization of
  "Structured Matrices Toolbox". A simple way to do this is to use Scilabs
  automatic module management system ATOMS (cf. http://atoms.scilab.org): start
  Scilab and execute the commands

    atomsSystemUpdate();
    atomsInstall(['lsitbx','1.0.1']);
    atomsInstall(['fsmtbx','0.1']);

  (You may want to replace the last two commands with

    atomsInstall lsitbx;
    atomsInstall fsmtbx;

  if you want to install the latest versions of the toolboxes. However, we
  cannot guarantee that this gives exactly the same results as in the paper.)
  The toolboxes should be available after Scilab has been restarted. Note that
  this procedure requires an active internet connection. Otherwise, please see
  http://lsitbx.origo.ethz.ch and http://fsmtbx.origo.ethz.ch for manual
  installation instructions.

3. Extract the content of this archive into some directory, e.g.,
  /home/john/source_thp [Linux] or c:\source_thp [Windows].

DETAILED RUN INSTRUCTIONS

1. Start Scilab

2. Make sure that the toolboxes are loaded (this step can be skipped if ATOMS
  has been used to install the toolboxes; cf. the set-up instructions)

3. Execute one of the scripts exa1.sce, exa2.sce or exa3.sce by, e.g., by
  execution of the Scilab command "cd /home/john/source_thp; exec exa1.sce"
  [Linux] or "cd c:\source_thp; exec exa1.sce" [Windows] (replace the directory
  with your chosen path). The scripts can be stopped and resumed because
  intermediate results are stored in data files of the form "exa*_data_SNR*.txt"
  ("*" can be anything). OTHER FILES IN THE DIRECTORY WHERE THE SIMULATION IS
  RUN MUST NOT FIT THIS SCHEME! Note that the data files have to be removed
  manually after simulation parameters have been changed.
  
OUTPUT DESCRIPTIONS

Each of the scripts exa1.sce, exa2.sce and exa3.sce create new windows which
contain the plots shown in the paper when they terminate. Please note that this
may take a long time. On Linux, multicore processors are exploited. Several
data files will be created while the simulation runs. (See the detailed run
instructions above.)

CONTACT INFORMATION

Email: sander.wahls@ieee.org
