function exa2a_base(nch)

    // This is a auxiliary function for the script exa2.sce. It performs
    // the actual simulation. Note: several global variables have to be set
    // before this script can be run (cf. loader.sce and exa2_params.sce).

    // initialize random number generator, salt ensures different seed per SNR
    rand("seed",salt+nch);

    // prepare the set of all permutation matrices for exhaustive search of the
    // optimal one; we only prepare the case q=3
    PIs = zeros(q,q,factorial(q));
    if q==3 then
        PIs(:,:,1) = [1 0 0; 0 1 0; 0 0 1];
        PIs(:,:,2) = [1 0 0; 0 0 1; 0 1 0];
        PIs(:,:,3) = [0 1 0; 1 0 0; 0 0 1];
        PIs(:,:,4) = [0 0 1; 0 1 0; 1 0 0];
        PIs(:,:,5) = [0 1 0; 0 0 1; 1 0 0];
        PIs(:,:,6) = [0 0 1; 1 0 0; 0 1 0];
    else
        error("TODO");
    end

    // noise power (per data stream)
    sigsq = Etr/10^(SNR/10)/q;

    // create IIR random channel and white noise model
    SYSH = syslin("d",rand(n,n,"normal")+%i*rand(n,n,"normal"), ...
    rand(n,p,"normal")+%i*rand(n,p,"normal"), ...
    rand(q,n,"normal")+%i*rand(q,n,"normal"), ...
    rand(q,p,"normal")+%i*rand(q,p,"normal"));
    SYSH.A = rand()*SYSH.A/max(abs(spec(SYSH.A)));
    SYSPSI = sqrt(sigsq/q)*syslin("d",[],[],[],eye(q,q));

    // compute the optimal THP for the permutation matrix PI=identity matrix
    [SYSP_noopt,al_noopt,SYST_noopt] = thp_iir(SYSH,SYSPSI,eye(q,q),sigv,Etr,L);

    // compute the optimal THP for the successively optimized permutation matrix
    [SYSP_succ,al_succ,SYST_succ,PI_succ] = thp_iir(SYSH,SYSPSI, ...
    "successive",sigv,Etr,L);

    // compute the optimal THP for the optimal permutation matrix found by
    // exhaustive search
    mse_opt = %inf;
    for k=1:factorial(q) do
        PI = PIs(:,:,k);
        [SYSP,al,SYST] = thp_iir(SYSH,SYSPSI,PI,sigv,Etr,L);
        mse = sigv^2*dh2norm(add_delay(PI'*(eye()-SYST),L)-al*SYSH*SYSP)^2 ...
        +al^2*dh2norm(SYSPSI)^2;
        if real(mse)<mse_opt then
            mse_opt = real(mse);
            SYSP_opt = SYSP;
            al_opt = al;
            SYST_opt = SYST;
            PI_opt = PI;
        end
    end

    // compute bit error rates
    ber_noopt = estimate_ber_qpsk(SYSH,SYSP_noopt,al_noopt, ...
    SYST_noopt,eye(q,q),L,sqrt(sigsq),tau,Nsymb);
    ber_succ = estimate_ber_qpsk(SYSH,SYSP_succ,al_succ, ...
    SYST_succ,PI_succ,L,sqrt(sigsq),tau,Nsymb);
    ber_opt = estimate_ber_qpsk(SYSH,SYSP_opt,al_opt, ...
    SYST_opt,PI_opt,L,sqrt(sigsq),tau,Nsymb);

    // add results to the data file
    line = sprintf("%e %e %e",ber_noopt,ber_succ,ber_opt);
    add_line_to_file(filename,line);

    // print status message
    printf("exa2a_base.sci: simulation of channel %5d of %5d at SNR=%3ddB "...
    +"completed\n",nch,nruns,SNR);

endfunction