function ber = estimate_ber_16qam(SYSH,SYSP,al,SYST,PI,L,sig_eta,tau,Nsymb)

    // Purpose:
    //
    //  Performs a simulation in order to estimate the uncoded bit error rate of
    //  a given Tomlinson-Harashima precoder for 16QAM modulated data vectors.
    //
    // Description:
    //
    //  The input parameters SYSH, SYSP and SYST are state-space realizations of
    //  the channel H(z), the precoder P(z) and the feedback filter T(z). The
    //  parameters al (a scalar), PI (a matrix), L (a natural) and tau (a
    //  scalar) are the scalar gain at the receiver, the permutation matrix, the
    //  latency time and the modulo constant. The input parameter sig_eta (a
    //  scalar) specifies the square root of the noise power per data stream and
    //  Nsymb (a natural) is the number of data vectors to be transmitted in the
    //  simulation.

    // extract channel dimensions
    [q,p] = size(SYSH);

    // create random 16QAM data vectors with avg. power one
    avg_pow = (4*(1.5^2+1.5^2)+8*(1.5^2+0.5^2)+4*(0.5^2+0.5^2))/16;
    s = ((floor(4*rand(q,Nsymb,"uniform"))-1.5)+ ...
    %i*(floor(4*rand(q,Nsymb,"uniform"))-1.5))/sqrt(avg_pow);

    // compute the according output of the Tomlinson-Harashima precoder
    Pv = encode_thp(SYSH,SYSP,SYST,PI,L,tau,s);

    // create noise vectors
    eta = sig_eta*(rand(q,Nsymb+L,"normal")+%i*rand(q,Nsymb+L,"normal")) ...
    /sqrt(2);

    // compute the received vectors
    y = cplxdsimul(SYSH,Pv)+eta;

    // apply the scalar gain and the modulo operator to the received signals
    shat = modulo_thp(al*y,tau);

    // map real and imaginary parts of s and shat to {1,2,3,4}
    s_real = round(real(s)*sqrt(avg_pow)+2.5);
    s_real = max(1,s_real);
    s_real = min(4,s_real);
    s_imag = round(imag(s)*sqrt(avg_pow)+2.5);
    s_imag = max(1,s_imag);
    s_imag = min(4,s_imag);
    shat_real = round(real(shat)*sqrt(avg_pow)+2.5);
    shat_real = max(1,shat_real);
    shat_real = min(4,shat_real);
    shat_imag = round(imag(shat)*sqrt(avg_pow)+2.5);
    shat_imag = max(1,shat_imag);
    shat_imag = min(4,shat_imag);

    // compute bit error rate under the assumption of the Gray code g(1)="00",
    // g(2)="01", g(3)="11", g(4)="10" in both the real and the imaginary part
    gray_code_bit1 = [0 0 1 1];
    gray_code_bit2 = [0 1 1 0];
    ber = 0;
    for k=1:q do
        ber = ber+...
        sum(gray_code_bit1(s_real(k,:))~=gray_code_bit1(shat_real(k,L+1:$)))+...
        sum(gray_code_bit1(s_imag(k,:))~=gray_code_bit1(shat_imag(k,L+1:$)))+...
        sum(gray_code_bit2(s_real(k,:))~=gray_code_bit2(shat_real(k,L+1:$)))+...
        sum(gray_code_bit2(s_imag(k,:))~=gray_code_bit2(shat_imag(k,L+1:$)));
    end
    ber = ber/Nsymb/q/4;

endfunction