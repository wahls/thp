function [F,G] = generator(SYSH,sqnrm_SYSPSI,Etr,L)

    // Purpose:
    //
    //  Computes a generator for the matrix PHI0red.
    //
    // Description:
    //
    //  We consider the matrix PHI0red, which is completely specified by the
    //  parameters SYSH (a state-space realization of the channel H(z)),
    //  sqnrm_SYSPSI (the squared norm of the noise model, a scalar), Etr
    //  (the transmit power, a scalar) and L (the latency time, a natural).
    //  The routine computes a generator for M, i.e., F and G are such that the
    //  displacement D=M-F'*M*F satisfies D=G*J*G' where J is the according
    //  signature matrix. The used generator is simplified version of the one
    //  given in the technical report "QR factorization of rank-deficient
    //  block Toeplitz matrices" by K. Gallivan, S. Thirumalai and P. Van Dooren
    //  (Univ. Illinois Urbana-Champaign, Coord. Sci. Lab., 1995)

    // extract channel dimensions
    [q,p] = size(SYSH);

    // compute the Cholesky factor R0
    H = impresp(SYSH,L);
    R0 = chol(sqnrm_SYSPSI/Etr*eye(p,p)+H'*H);

    // compute Y
    Y = zeros(p,(L-1)*p);
    for k=1:L-1 do
        Y(:,(k-1)*p+1:k*p) = H(q+1:$,:)'*[zeros((k-1)*q,p) ; H(1:$-k*q,:)];
    end

    // compute G
    G3 = [zeros(p,p) (R0')\Y];
    G1 = G3;
    G1(1:p,1:p) = R0;
    G4 = zeros(q,L*p);
    for k=1:L-1 do
        G4(:,k*p+1:(k+1)*p) = H((L-k)*q+1:(L-k+1)*q,:);
    end
    G = [G1' G3' G4']';

    // compute F (use sparse format because of ldl_gs)
    F = [spzeros(p,L*p) ; speye((L-1)*p,(L-1)*p) spzeros((L-1)*p,p)];

endfunction