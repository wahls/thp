function v = modulo_thp(u,tau)

    // Purpose:
    //
    //  Implements the modulo operator.
    //
    // Description:
    //
    //  The input u is a matrix and tau (a scalar) is the modulo constant. The
    //  output v is the result of the component-wise application of the
    //  aforementioned modulo operator to u.

    v = u-tau*floor(real(u)/tau+0.5)-%i*tau*floor(imag(u)/tau+0.5);

endfunction